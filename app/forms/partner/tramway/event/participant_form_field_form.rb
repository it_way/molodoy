# frozen_string_literal: true

class Partner::Tramway::Event::ParticipantFormFieldForm < ::Tramway::Core::ExtendedApplicationForm
  properties :title, :description, :field_type, :options, :position, :presence_field, :select_options
  association :event

  def initialize(object)
    super(object).tap do
      form_properties select_options: {
        type: :string,
        input_options: {
          hint: I18n.t('hints.tramway.event.participant_form_field.select_options')
        }
      }
    end
  end

  def submit(params)
    super(params).tap do
      model.options = {} if model.options == ''
      model.options&.deep_merge! validations: { presence: (params[:presence_field] == '1').to_s }
      model.save
    end
  end

  def presence_field
    model.options&.dig('validations', 'presence') == 'true'
  end
end
