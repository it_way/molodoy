# frozen_string_literal: true

class Partner::Tramway::User::UserForm < Tramway::Core::ApplicationForm
  properties :email, :password, :first_name, :last_name, :phone

  def initialize(object)
    super(object).tap do
      form_properties email: :string,
                      password: :string,
                      first_name: :string,
                      last_name: :string,
                      phone: :string
    end
  end
end
