# frozen_string_literal: true

class Public::UserSignUpForm < Tramway::Auth::SignUpForm
  self.model_class = User
  self.sign_in_after = false

  properties :first_name, :last_name, :email, :password

  def initialize(object)
    super(object).tap do
      self.submit_message = 'Регистрация'
      form_properties email: :string, first_name: :string, last_name: :string, password: :default
    end
  end
end
