# frozen_string_literal: true

class Public::UserSignInForm < Tramway::Auth::SessionForm
  def initialize(object)
    super(object).tap do
      self.submit_message = 'Войти'
      form_properties email: :string, password: :default
    end
  end
end
