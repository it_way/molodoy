# frozen_string_literal: true

class Admin::UserForm < Tramway::Core::ApplicationForm
  properties :first_name, :last_name, :email, :password

  def initialize(object)
    super(object).tap do
      form_properties email: :string,
                      password: :default,
                      first_name: :string,
                      last_name: :string
    end
  end
end
