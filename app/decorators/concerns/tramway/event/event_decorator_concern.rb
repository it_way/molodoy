# frozen_string_literal: true

module Tramway::Event::EventDecoratorConcern
  extend ActiveSupport::Concern

  included do
    def self.list_attributes
      %i[requested_participants approved_participants events_link creator_name]
    end

    def creator_name
      return unless object.creator.present?

      "#{object.creator.class}Decorator".constantize.decorate(object.creator).name
    end
  end
end
