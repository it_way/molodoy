# frozen_string_literal: true

module Tramway::User::UserDecoratorConcern
  extend ActiveSupport::Concern

  included do
    def self.show_attributes
      %i[email first_name last_name phone role created_at updated_at events_list]
    end

    def self.show_associations
      [:social_networks]
    end

    def events_list
      content_tag :table do
        ::Tramway::Event::Event.active.created_by_user(object.id).each do |event|
          concat(content_tag(:tr) do
            content_tag :td do
              content_tag(:a, href: admin_event_record_path(event)) { event.title }
            end
          end)
        end
      end
    end

    decorate_association :social_networks, as: :record

    private

    def admin_event_record_path(event)
      ::Tramway::Admin::Engine.routes.url_helpers.record_path(event.id, model: ::Tramway::Event::Event)
    end
  end
end
