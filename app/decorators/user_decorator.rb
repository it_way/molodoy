# frozen_string_literal: true

class UserDecorator < Tramway::Core::ApplicationDecorator
  class << self
    def list_attributes
      %i[title email]
    end
  end

  def title
    "#{object&.first_name} #{object&.last_name}"
  end

  delegate_attributes :email
end
