# frozen_string_literal: true

class Web::WelcomeController < ApplicationController
  include Tramway::AuthManagement
  layout 'tramway/landing/application'

  before_action :application

  def index
    @blocks = ::Tramway::Landing::BlockDecorator.decorate ::Tramway::Landing::Block.on_main_page
    @close_events = close_events
    @header_with_form = {
      user_sign_up: Public::UserSignUpForm.new(User.new),
      user_sign_in: Public::UserSignInForm.new(User.new)
    }.with_indifferent_access
    @signed_in = signed_in? User
    return unless @signed_in

    @links = build_links
  end

  private

  def application
    @application = ::Tramway::Core.application_object
  end

  def close_events
    events = ::Tramway::User::User.active.map do |partner|
      ::Tramway::Event::Event.active.actual.open.created_by_user(partner.id).first
    end.compact
    events.sort_by(&:begin_date).map do |event|
      EventAsPageWithButtonDecorator.decorate event
    end
  end

  def build_links
    {
      right: [{
        current_user(User).name => [
          Tramway::Landing::Navbar::LinkDecorator.new(
            title: 'Выйти',
            link: Tramway::Auth::Engine.routes.url_helpers.sign_out_path(model: User)
          )
        ]
      }]
    }
  end
end
