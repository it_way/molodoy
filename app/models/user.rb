# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  validates :email, email: true, uniqueness: { conditions: -> { active } }
  validates :first_name, presence: true
  validates :last_name, presence: true
end
