# frozen_string_literal: true

module Tramway::User::UserConcern
  extend ActiveSupport::Concern

  included do
    scope :partner_scope, ->(partner_id) { where(id: partner_id) }
    scope :partners, -> { where role: :partner }

    has_many :social_networks, as: :record, class_name: 'Tramway::Profiles::SocialNetwork'

    enumerize :role, in: %i[admin partner], default: :admin

    def admin?
      role.in? %w[admin partner]
    end

    def title
      "#{first_name} #{last_name}"
    end
  end
end
