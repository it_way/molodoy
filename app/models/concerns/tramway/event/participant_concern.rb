# frozen_string_literal: true

module Tramway::Event::ParticipantConcern
  extend ActiveSupport::Concern

  included do
    scope :partner_scope, lambda { |partner_id|
      joins(event: :audits).where(
        'tramway_event_events.state = \'active\' AND audits.action = \'create\' AND audits.user_id = ?',
        partner_id
      )
    }
  end
end
