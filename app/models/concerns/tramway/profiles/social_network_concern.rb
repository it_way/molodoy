# frozen_string_literal: true

module Tramway::Profiles::SocialNetworkConcern
  extend ActiveSupport::Concern

  included do
    scope :partner_scope, lambda { |partner_id|
      where record_type: 'Tramway::User::User', record_id: partner_id
    }
  end
end
