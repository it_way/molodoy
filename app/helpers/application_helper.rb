# frozen_string_literal: true

module ApplicationHelper
  include ::Tramway::Landing::ApplicationHelper
  include ::Tramway::Collections::Helper
  include ::Collections
end
