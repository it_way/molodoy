# frozen_string_literal: true

module ::Tramway::Extensions
  def self.load
    Tramway::Event::Event.include Tramway::Event::EventConcern
    Tramway::Event::Action.include Tramway::Event::ActionConcern
    Tramway::Event::Participant.include Tramway::Event::ParticipantConcern
    Tramway::Event::ParticipantFormField.include Tramway::Event::ParticipantFormFieldConcern
    Tramway::User::User.include Tramway::User::UserConcern
    Tramway::Profiles::SocialNetwork.include Tramway::Profiles::SocialNetworkConcern
    Tramway::User::UserDecorator.include Tramway::User::UserDecoratorConcern
    Tramway::Event::EventDecorator.include Tramway::Event::EventDecoratorConcern
  end
end

Tramway::Event::Event.include Tramway::Event::EventConcern
Tramway::Event::Participant.include Tramway::Event::ParticipantConcern
Tramway::Event::ParticipantFormField.include Tramway::Event::ParticipantFormFieldConcern
Tramway::User::User.include Tramway::User::UserConcern
Tramway::User::UserDecorator.include Tramway::User::UserDecoratorConcern
Tramway::Event::EventDecorator.include Tramway::Event::EventDecoratorConcern
Tramway::Event::Action.include Tramway::Event::ActionConcern
Tramway::Profiles::SocialNetwork.include Tramway::Profiles::SocialNetworkConcern
