# frozen_string_literal: true

require 'octokit'
require 'kramdown'

::Tramway::Landing.head_content = (lambda do
  concat javascript_include_tag :application
  concat stylesheet_link_tag :application
end)
