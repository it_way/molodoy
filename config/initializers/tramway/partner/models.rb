# frozen_string_literal: true

::Tramway::Admin.set_available_models(
  ::Tramway::Event::Event,
  {
    ::Tramway::Event::ParticipantFormField => [:index, :show, :create, :update, { destroy: lambda do |record|
      mandatory_fields = ::Tramway::Event::EventConcern::MANDATORY_FIELDS.values.map do |field|
        field[:title]
      end
      !record.model.title.in?(mandatory_fields)
    end }]
  },
  ::Tramway::Event::Participant,
  ::Tramway::Event::Action,
  {
    Tramway::User::User => [
      :index,
      {
        show: lambda do |record, current_user|
          record.id == current_user.id
        end
      },
      {
        update: lambda do |record, current_user|
          record.id == current_user.id
        end
      }
    ]
  },
  {
    Tramway::Profiles::SocialNetwork => [
      :index,
      {
        show: lambda do |record, current_user|
          record.record_type == 'Tramway::User::User' && record.record_id == current_user.id
        end
      },
      {
        update: lambda do |record, current_user|
          record.record_type == 'Tramway::User::User' && record.record_id == current_user.id
        end
      }
    ]
  },
  project: :molodoy,
  role: :partner
)
