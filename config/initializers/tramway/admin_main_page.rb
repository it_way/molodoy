# frozen_string_literal: true

::Tramway::Admin.welcome_page_actions = lambda do
  if Rails.env.production?
    client = Octokit::Client.new access_token: Rails.application.secrets[:github_access_token]
    url = client.contents(
      'ulmic/tramway-dev',
      path: 'tramway-event/docs/russian/README.md',
      query: { ref: 'develop' }
    )[:download_url]
    markdown = Net::HTTP.get(URI.parse(url)).force_encoding 'utf-8'
    @content = Kramdown::Document.new(markdown).to_html
  end
end
