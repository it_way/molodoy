# frozen_string_literal: true

Tramway::Admin.navbar_structure(
  Tramway::Event::Event,
  Tramway::Event::Participant,
  Tramway::Event::Action,
  {
    admin: [
      Tramway::Page::Page,
      Tramway::User::User,
      User,
      Tramway::Profiles::SocialNetwork,
      Tramway::Landing::Form
    ]
  },
  project: :molodoy
)
