# frozen_string_literal: true

::Tramway::Admin.set_available_models ::Tramway::Event::Event, ::Tramway::Event::ParticipantFormField,
  ::Tramway::Event::Participant, ::Tramway::Landing::Block, ::Tramway::User::User, ::Tramway::Event::Action,
  ::Tramway::Profiles::SocialNetwork, Tramway::Page::Page, User, Tramway::Landing::Form, project: :molodoy, role: :admin
