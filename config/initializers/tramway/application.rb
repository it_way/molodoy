# frozen_string_literal: true

::Tramway::Core.initialize_application name: :molodoy,
                                       tagline: 'Конструктор для создания публичных страниц ваших мероприятий',
                                       short_description: 'Регистрация позволяет быстро заявляться на мероприятия',
                                       title: 'MOLODOY.ONLINE',
                                       phone: '+79378806332',
                                       email: 'yes_man73@mail.ru',
                                       main_image: 'main_image.png',
                                       favicon: '/icon.ico'
Audited.current_user_method = :authenticated_user
