# frozen_string_literal: true

Tramway::Export.set_exportable_models Tramway::Event::Participant, project: :molodoy
