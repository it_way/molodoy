# frozen_string_literal: true

::Tramway::Auth.root_path_for Tramway::User::User => '/admin', User => '/'
