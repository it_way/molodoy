# frozen_string_literal: true

Configus.build Rails.env do
  env :production do
    host 'https://molodoy.online'
  end
  env :development, parent: :production do
    host 'http://localhost:3000'
  end
  env :test, parent: :development
end
