# frozen_string_literal: true

require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
require 'sprockets/railtie'
Bundler.require(*Rails.groups)

module Molodoy
  class Application < Rails::Application
    config.load_defaults 5.1
    config.i18n.available_locales = [:ru]
    config.i18n.default_locale = :ru
    config.generators.system_tests = nil
    config.autoload_paths += Dir["#{config.root}/lib/**/"]
    config.time_zone = 'Samara'

    Tramway::Profiles.records = ['Tramway::User::User']
  end
end
