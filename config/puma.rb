# frozen_string_literal: true

threads_count = ENV.fetch('RAILS_MAX_THREADS') { 5 }
threads threads_count, threads_count
port        ENV.fetch('APP_PORT') { 3000 }
environment ENV.fetch('RAILS_ENV') { 'development' }
plugin :tmp_restart
bind 'unix:///tmp/puma.sock' if %w[production dev].include? ENV.fetch('RAILS_ENV')
