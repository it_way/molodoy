# frozen_string_literal: true

Rails.application.routes.draw do
  mount Tramway::Auth::Engine, at: '/auth'
  mount Tramway::Admin::Engine, at: '/admin'
  mount Tramway::Event::Engine, at: '/'

  root to: 'web/welcome#index'

  scope module: :web do
    resource :session, only: %i[new create destroy]
  end
end
