class AddUrlToTramwayLandingForms < ActiveRecord::Migration[5.1]
  def change
    add_column :tramway_landing_forms, :url, :text
  end
end
