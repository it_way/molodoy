class UpdateRecordIdToTramwayProfilesSocialNetworks < ActiveRecord::Migration[5.1]
  def change
  	remove_column :tramway_profiles_social_networks, :record_id
  	add_column :tramway_profiles_social_networks, :record_id, :integer
  end
end
