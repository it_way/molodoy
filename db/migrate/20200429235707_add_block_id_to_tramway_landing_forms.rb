class AddBlockIdToTramwayLandingForms < ActiveRecord::Migration[5.1]
  def change
    add_column :tramway_landing_forms, :block_id, :integer
  end
end
