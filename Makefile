dev_install:
	rvm install ruby-2.7.0
	rvm use ruby-2.7.0
	gem install bundler
	bundle

production_install:
	ansible-galaxy install ANXS.postgresql
	ansible-playbook -v -i deploy/inventory deploy/production/install.yml
	ansible-playbook -v -i deploy/inventory deploy/production/prepare_directories.yml
production_deploy:
	ansible-playbook -v -i deploy/inventory deploy/production/deploy.yml
production_restart:
	ansible-playbook -v -i deploy/inventory deploy/production/restart.yml

ssh_production:
	ssh nuntiux@37.228.116.224

production_logs:
	ssh -t nuntiux@37.228.116.224 'tail -f backend/current/log/production.log'

build_test:
	docker-compose -f docker-compose.tests.yml build

test:
	docker-compose -f docker-compose.tests.yml run --entrypoint ci/entrypoint.sh test-backend bash -c 'xvfb-run -a --server-args="-screen 0 1920x1080x8" bundle exec rspec'

rubocop:
	docker-compose -f docker-compose.tests.yml run --entrypoint ci/entrypoint.sh test-backend bash -c 'bundle exec rubocop'
