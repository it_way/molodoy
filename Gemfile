# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '2.7.1'
gem 'rails', '~> 5.1.7'

# gem 'tramway-admin', path: '../tramway-admin'
# gem 'tramway-auth', path: '../tramway-dev/tramway-auth'
# gem 'tramway-core', path: '../tramway-core'
# gem 'tramway-event', path: '../tramway-dev/tramway-event'
# gem 'tramway-export', path: '../tramway-dev/tramway-export'
# gem 'tramway-landing', path: '../tramway-dev/tramway-landing'
# gem 'tramway-page', path: '../tramway-dev/tramway-page'
# gem 'tramway-profiles', path: '../tramway-dev/tramway-profiles'
# gem 'tramway-user', path: '../tramway-dev/tramway-user'

gem 'tramway-admin', '1.32.1.3'
gem 'tramway-auth', '2.0.2'
gem 'tramway-core', '1.18.5.2'
gem 'tramway-event', '1.12.3.9'
gem 'tramway-export', '0.1.6.1'
gem 'tramway-landing', '3.1.1.10'
gem 'tramway-page', '1.5.3.4'
gem 'tramway-profiles', '1.4.1.1'
gem 'tramway-user', '2.1.3.3'

gem 'audited'
gem 'bcrypt'
gem 'bootstrap', '~> 4.4.1'
gem 'bootstrap-kaminari-views', github: 'kalashnikovisme/bootstrap-kaminari-views', branch: :master
gem 'carrierwave'
gem 'ckeditor', '4.2.4'
gem 'clipboard-rails'
gem 'configus'
gem 'copyright_mafa'
gem 'haml-rails'
gem 'jquery-rails'
gem 'kaminari'
gem 'kramdown'
gem 'more_html_tags', '>= 0.2.0'
gem 'octokit', '>= 4.18.0'
gem 'ransack'
gem 'roo-xls'
gem 'selectize-rails'
gem 'sentry-raven'
gem 'smart_buttons', '1.0.0.1'
gem 'state_machine', github: 'seuros/state_machine'
gem 'state_machine_buttons', '1.0'
gem 'trap', '3.0'
gem 'validates'

gem 'jbuilder', '~> 2.5'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'dotenv-rails'
  gem 'pry'
  gem 'rubocop'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'selenium-webdriver'
  gem 'time_difference'
  gem 'webdrivers', '>= 2.1.0'
  gem 'webmock'
  gem 'whenever-test'

  gem 'capybara', '>= 2.15'
  gem 'factory_bot_rails', '~> 4.0'
  gem 'faker'
  gem 'json_api_test_helpers', '1.2'
  gem 'json_matchers'
  gem 'rspec-json_expectations', '2.2.0'
  gem 'rspec-rails', '~> 3.5'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
