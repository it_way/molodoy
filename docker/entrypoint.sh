#!/bin/bash

cat > config/database.yml <<EOF

default: &default
  adapter: postgresql
  encoding: utf8
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: <%= ENV.fetch("DB_HOST") { } %>
  username: <%= ENV.fetch("POSTGRES_USER") { } %>
  password: <%= ENV.fetch("POSTGRES_PASSWORD") { } %>
  database: <%= ENV.fetch("POSTGRES_DB") { } %>
  port: <%= ENV.fetch("DB_PORT") { } %>
  timeout: 5000

development:
  <<: *default


production:
  <<: *default


test:
  <<: *default

staging:
  <<: *default

EOF

bundle check || bundle install --binstubs="$BUNDLE_BIN"
bundle exec rails db:migrate
bundle exec rake assets:clean assets:precompile

exec "$@"
