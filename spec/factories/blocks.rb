# frozen_string_literal: true

FactoryBot.define do
  factory :block, class: Tramway::Landing::Block do
    title
    position { generate :integer }
    block_type { Tramway::Landing::Block.block_type.values.sample }
    background { generate :image_as_file }
    navbar_link { Tramway::Landing::Block.navbar_link.values.sample }
    anchor { generate :string }
    description { generate :string }
    view_name { generate :string }
    page

    trait :on_main_page do
      page { Tramway::Page::Page.where(page_type: :main).last || create(:page, page_type: :main) }
    end
  end

  factory :block_admin_attributes, class: Tramway::Landing::Block do
    title
    position { generate :integer }
    block_type { Tramway::Landing::Block.block_type.values.sample.text }
    background { generate :image_as_file }
    navbar_link { Tramway::Landing::Block.navbar_link.values.sample.text }
    anchor { generate :string }
    view_name { generate :string }
    page { (Tramway::Page::Page.last || create(:page)).title }
  end

  factory :block_header_with_form, parent: :block, class: Tramway::Landing::Block do
    block_type { :header_with_form }
    values { { form_url: '/auth/sign_up' } }
  end
end
