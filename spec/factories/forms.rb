# frozen_string_literal: true

FactoryBot.define do
  factory :form, class: 'Tramway::Landing::Form' do
    title
    form_name { Tramway::Landing::Form.form_name.values.sample }
    block
    position { generate :integer }

    trait :sign_up do
      form_name { 'user_sign_up' }
      url 'auth/sign_up'
    end

    trait :sign_in do
      form_name { 'user_sign_in' }
      url 'auth/session'
    end
  end
end
