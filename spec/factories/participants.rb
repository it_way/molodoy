# frozen_string_literal: true

FactoryBot.define do
  factory :participant, class: 'Tramway::Event::Participant' do
    values do
      {
        'Фамилия' => generate(:string),
        'Имя' => generate(:string),
        'Отчество' => generate(:string),
        'Дата рождения' => generate(:date).strftime('%d.%m.%Y'),
        'Муниципальное образование' => Collections::Municipalities.list.sample,
        'Место учёбы / работы' => generate(:string),
        'Email' => generate(:email),
        'Номер телефона' => generate(:phone),
        'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт' => generate(:string),
        'Я согласен на обработку моих персональных данных' => 'true'
      }
    end
    event_id { Tramway::Event::Event.last&.id || create(:event).id }
  end

  factory :participant_default_event_attributes, class: 'Tramway::Event::Participant' do
    send('Фамилия') { generate(:string) }
    send('Имя') { generate(:string) }
    send('Отчество') { generate(:string) }
    send('Дата рождения') { generate(:date).strftime('%d.%m.%Y') }
    send('Муниципальное образование') { Collections::Municipalities.list.sample }
    send('Место учёбы / работы') { generate(:string) }
    send('Email') { generate(:email) }
    send('Номер телефона') { generate(:phone) }
    send('Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт') { generate(:string) }
    send('Я согласен на обработку моих персональных данных') { 'true' }
  end

  factory :participant_admin_attributes, parent: :participant_default_event_attributes
end
