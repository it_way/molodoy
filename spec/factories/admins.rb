# frozen_string_literal: true

FactoryBot.define do
  factory :admin, class: Tramway::User::User do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
    phone { generate :phone }
    role { :admin }

    trait :with_social_networks do
      after :create do |user|
        Tramway::Profiles::SocialNetwork.network_name.values.each do |network|
          create :social_network, network_name: network, record_type: Tramway::User::User, record_id: user.id
        end
      end
    end
  end

  factory :partner, parent: :admin, class: Tramway::User::User do
    first_name { generate :string }
    last_name { generate :string }
    role { :partner }
  end

  factory :admin_admin_attributes, class: Tramway::User::User do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
    phone { generate :phone }
    role { %w[Администратор Партнер].sample }
  end

  factory :partner_admin_attributes, class: Tramway::User::User do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
    phone { generate :phone }
  end
end
