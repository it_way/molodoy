# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
  end

  factory :user_sign_up_attributes, class: 'User' do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
  end

  factory :user_sign_in_attributes, class: 'User' do
    email
    password
  end

  factory :user_admin_attributes, class: 'User' do
    email
    password
    first_name { generate :string }
    last_name { generate :string }
  end
end
