# frozen_string_literal: true

FactoryBot.define do
  trait :campaign_started do
    begin_date { (DateTime.now + 1.day).beginning_of_day.change(offset: '+0400') }
    end_date { (DateTime.now + 2.day).beginning_of_day.change(offset: '+0400') }
    request_collecting_begin_date { (DateTime.now - 1.day).beginning_of_day.change(offset: '+0400') }
    request_collecting_end_date { (DateTime.now + 10.days).beginning_of_day.change(offset: '+0400') }
  end

  factory :event, class: 'Tramway::Event::Event' do
    title { generate :string }
    description { generate :string }
    short_description { generate :string }
    begin_date { generate(:date) }
    end_date { begin_date + 10.days }
  end

  factory :event_created_by_partner, parent: :event do
    after :create do |event|
      creation_event = event.audits.where(action: :create).first
      creation_event.update!(
        user_id: Tramway::User::User.active.where(role: :partner).last.id,
        user_type: Tramway::User::User
      )
    end

    trait :with_full_filled_partner do
      after :create do |event|
        creation_event = event.audits.where(action: :create).first
        creation_event.update! user_id: create(:partner, :with_social_networks).id, user_type: Tramway::User::User
      end
    end
  end

  factory :event_created_by_admin, parent: :event do
    after :create do |event|
      creation_event = Audited::Audit.where(auditable: event, action: :create).first
      creation_event.update!(
        user_id: Tramway::User::User.active.where(role: :admin).last.id,
        user_type: Tramway::User::User
      )
    end

    trait :with_full_filled_admin do
      after :create do |event|
        creation_event = event.audits.where(action: :create).first
        creation_event.update! user_id: create(:admin, :with_social_networks).id, user_type: Tramway::User::User
      end
    end
  end

  factory :event_admin_attributes, class: 'Tramway::Event::Event' do
    title { generate :string }
    begin_date { DateTime.now.beginning_of_day.change(offset: '+0400').strftime('%d.%m.%Y') }
    end_date { DateTime.now.beginning_of_day.change(offset: '+0400').strftime('%d.%m.%Y') }
    request_collecting_begin_date { DateTime.now.beginning_of_day.change(offset: '+0400').strftime('%d.%m.%Y') }
    request_collecting_end_date { DateTime.now.beginning_of_day.change(offset: '+0400').strftime('%d.%m.%Y') }
    reach { Tramway::Event::Event.reach.values.sample.text }
  end
end
