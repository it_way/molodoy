# frozen_string_literal: true

FactoryBot.define do
  factory :social_network, class: Tramway::Profiles::SocialNetwork do
    title
    network_name { Tramway::Profiles::SocialNetwork.network_name.values.sample }
    uid { generate :string }
  end

  factory :social_network_admin_attributes, class: 'Tramway::Profiles::SocialNetwork' do
    title
    uid { generate :string }
    network_name { Tramway::Profiles::SocialNetwork.network_name.values.sample.text }
    record do
      record_type = Tramway::Profiles::SocialNetwork.record_type.values.sample
      record_title = record_type.constantize.last&.title || create(record_type.underscore).title
      "#{record_type.constantize.model_name.human} | #{record_title}"
    end
  end

  factory :social_network_created_by_partner, parent: :social_network do
    record_type { 'Tramway::User::User' }
    record_id { (Tramway::User::User.partners.last || create(:partner)).id }
  end
end
