# frozen_string_literal: true

require 'rails_helper'

describe 'Show event' do
  context 'Collecting requests campaign started' do
    let(:event) { create :event_created_by_partner, :campaign_started }

    it 'should show event' do
      visit "/events/#{event.id}"

      expect(page).to have_content event.title
    end

    it 'should show participants form' do
      visit "/events/#{event.id}"

      event.participant_form_fields.each do |form_field|
        expect(page).to have_field "tramway_event_participant[#{form_field.title}]"
      end
    end

    context 'without mandatory fields' do
      it 'participants form fields are not checked with asterix' do
        event.participant_form_fields.update_all options: { validations: { presence: false } }
        visit "/events/#{event.id}"

        expect(page).not_to have_content '*'
      end
    end
  end

  context 'Collecting requests campaign is not started' do
    let(:event) { create :event_created_by_partner }

    it 'should show event' do
      visit "/events/#{event.id}"

      expect(page).to have_content event.title
    end

    it 'should not show participants form' do
      visit "/events/#{event.id}"

      expect(page).not_to have_xpath 'form.tramway_event_participant'
    end
  end

  context 'Responsible person contacts' do
#    Tramway::User::User.role.values.each do |role|
#      let(:event) { create "event_created_by_#{role}" }
#
#      it 'should show full name of responsible person' do
#        visit "/events/#{event.id}"
#
#        decorated_creator = Tramway::User::UserDecorator.decorate event.creator
#
#        expect(page).to have_content decorated_creator.name
#      end
#
#      it 'should show phone of responsible person' do
#        visit "/events/#{event.id}"
#
#        expect(page).to have_content event.creator.phone
#      end
#
#      it 'should show email of responsible person' do
#        visit "/events/#{event.id}"
#
#        expect(page).to have_content event.creator.email
#      end
#
#      context 'with social_networks' do
#        Tramway::Profiles::SocialNetwork.network_name.values.each do |network|
#          let(:event) { create "event_created_by_#{role}", "with_full_filled_#{role}".to_sym }
#
#          it "should show #{network} of responsible #{role}" do
#            visit "/events/#{event.id}"
#
#            expect(page).to(
#              have_content(event.creator.social_networks.where(network_name: network).first.title)
#            )
#          end
#        end
#      end
#    end
  end
end
