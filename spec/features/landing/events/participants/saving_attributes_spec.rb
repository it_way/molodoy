# frozen_string_literal: true

require 'rails_helper'

describe 'Creating participant' do
  let(:event) { create :event_created_by_partner, :campaign_started }
  let(:attributes) { attributes_for :participant_default_event_attributes }
  let(:region_field_name) do
    'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт'
  end

  context 'Error' do
    it 'saving participant data in the form' do
      visit "/events/#{event.id}"

      fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
      fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
      click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
      select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
      fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
      fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
      fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
      fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]

      click_on 'Отправить заявку'

      expect(page).to have_field('tramway_event_participant[Имя]', with: attributes[:'Имя'])
      expect(page).to have_field('tramway_event_participant[Отчество]', with: attributes[:'Отчество'])
      expect(page).to have_field('tramway_event_participant[Дата рождения]', with: attributes[:'Дата рождения'])
      expect(page).to(
        have_select(
          'tramway_event_participant[Муниципальное образование]',
          selected: attributes[:'Муниципальное образование']
        )
      )
      expect(page).to(
        have_field('tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы'])
      )
      expect(page).to have_field('tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона'])
      expect(page).to have_field('tramway_event_participant[Email]', with: attributes[:Email])
      expect(page).to(
        have_field("tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"])
      )
    end
  end
end
