# frozen_string_literal: true

require 'rails_helper'

describe 'Creating participant with errors' do
  let(:event) { create :event_created_by_partner, :campaign_started }
  let(:attributes) { attributes_for :participant_default_event_attributes }
  let(:region_field_name) do
    'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт'
  end

  it 'should show first_name error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show second_name error' do
    visit "/events/#{event.id}"

    event.participant_form_fields.map  do |e|
      e.options = e.options.merge('validations' => { 'presence' => 'true' })
      e.save
    end

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show last_name error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show birth_date error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show organization error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show email error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show phone error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show personal_data_info error' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]

    click_on 'Отправить заявку'

    expect(page).to have_content 'этот пункт должен быть отмечен'
  end
end
