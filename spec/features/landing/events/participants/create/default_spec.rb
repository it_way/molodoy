# frozen_string_literal: true

require 'rails_helper'

describe 'Creating participant' do
  let(:event) { create :event_created_by_partner, :campaign_started }
  let(:attributes) { attributes_for :participant_default_event_attributes }
  let(:region_field_name) do
    'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт'
  end

  it 'should create participant' do
    count = Tramway::Event::Participant.count
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(Tramway::Event::Participant.count).to eq count + 1
  end

  it 'should create participant and show events page' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    expect(page).to have_content 'Заявка отправлена успешно!'
  end

  it 'should create participant with needed arguments' do
    visit "/events/#{event.id}"

    fill_in 'tramway_event_participant[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'tramway_event_participant[Имя]', with: attributes[:'Имя']
    fill_in 'tramway_event_participant[Отчество]', with: attributes[:'Отчество']
    click_on_date_picker_date 'tramway_event_participant[Дата рождения]', value: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'tramway_event_participant[Муниципальное образование]'
    fill_in 'tramway_event_participant[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'tramway_event_participant[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'tramway_event_participant[Email]', with: attributes[:Email]
    fill_in "tramway_event_participant[#{region_field_name}]", with: attributes[:"#{region_field_name}"]
    check 'tramway_event_participant_Я согласен на обработку моих персональных данных'

    click_on 'Отправить заявку'

    participant = Tramway::Event::Participant.last

    attributes.each do |pair|
      actual = participant.values[pair[0].to_s]
      expecting = pair[1]
      expecting = expecting.strftime('%d.%m.%Y') if expecting.is_a? DateTime
      expect(actual).to eq(expecting), problem_with(attr: pair[0], expecting: expecting, actual: actual)
    end
  end
end
