# frozen_string_literal: true

require 'rails_helper'

describe 'Sign in user as second form' do
  let!(:block) { create :block_header_with_form, :on_main_page, view_state: :published }
  let!(:sign_up_form) { create :form, :sign_up, block: block, position: 1 }
  let!(:sign_in_form) { create :form, :sign_in, block: block, position: 2 }
  let!(:user) { create :user, password: '123' }

  it 'should sign in user' do
    visit '/'

    click_on sign_in_form.title

    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: '123'

    click_on 'Войти'

    expect(page.html).to include "#{user.first_name} #{user.last_name}"
  end
end
