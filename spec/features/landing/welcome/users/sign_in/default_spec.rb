# frozen_string_literal: true

require 'rails_helper'

describe 'Sign in user' do
  before do
    block = create :block_header_with_form, :on_main_page, view_state: :published
    create :form, :sign_in, block: block
  end
  let(:user) { create :user, password: '123' }

  it 'should sign in user' do
    visit '/'

    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: '123'

    click_on 'Войти'

    expect(page.html).to include "#{user.first_name} #{user.last_name}"
  end
end
