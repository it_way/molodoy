# frozen_string_literal: true

require 'rails_helper'

describe 'Creating user' do
  before do
    block = create :block_header_with_form, :on_main_page, view_state: :published
    create :form, :sign_up, block: block
  end
  let(:attributes) { attributes_for :user_sign_up_attributes }

  it 'should create user' do
    count = User.count
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[last_name]', with: attributes[:last_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    expect(User.count).to eq count + 1
  end

  it 'should create user and show blocks page' do
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[last_name]', with: attributes[:last_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    expect(page).to have_content 'Регистрация прошла успешно'
  end

  it 'should create user with needed arguments' do
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[last_name]', with: attributes[:last_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    user = User.last

    attributes.each do |pair|
      next if pair[0] == :password

      actual = user.send(pair[0].to_s)
      expecting = pair[1]
      expect(actual).to eq(expecting), problem_with(attr: pair[0], expecting: expecting, actual: actual)
    end
  end
end
