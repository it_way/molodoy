# frozen_string_literal: true

require 'rails_helper'

describe 'Errors while sign up' do
  before do
    block = create :block_header_with_form, :on_main_page, view_state: :published
    create :form, :sign_up, block: block
  end
  let(:attributes) { attributes_for :user_sign_up_attributes }

  it 'should show email is not valid error' do
    visit '/'

    fill_in 'user[email]', with: 'not_valid_email'
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[last_name]', with: attributes[:last_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    expect(page).to have_content 'имеет неверный формат'
  end

  it 'should show first_name not exists error' do
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[last_name]', with: attributes[:last_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    expect(page).to have_content 'не может быть пустым'
  end

  it 'should show last_name not exists error' do
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[password]', with: attributes[:password]

    click_on 'Регистрация'

    expect(page).to have_content 'не может быть пустой'
  end

  it 'should show password not exists error' do
    visit '/'

    fill_in 'user[email]', with: attributes[:email]
    fill_in 'user[first_name]', with: attributes[:first_name]
    fill_in 'user[last_name]', with: attributes[:last_name]

    click_on 'Регистрация'

    expect(page).to have_content 'не может быть пустым'
  end
end
