# frozen_string_literal: true

require 'rails_helper'

describe 'Creating participant' do
  before do
    block = create :block_header_with_form, :on_main_page, view_state: :published
    create :form, :sign_up, block: block
  end
  let(:attributes) { attributes_for :user_sign_up_attributes }

  context 'Error' do
    it 'saving participant data in the form' do
      visit '/'

      fill_in 'user[email]', with: 'not_valid_email'
      fill_in 'user[first_name]', with: attributes[:first_name]
      fill_in 'user[last_name]', with: attributes[:last_name]
      fill_in 'user[password]', with: attributes[:password]

      click_on 'Регистрация'

      expect(page).to have_field('user[email]'), with: 'not_valid_email'
      expect(page).to have_field('user[first_name]'), with: attributes[:first_name]
      expect(page).to have_field('user[last_name]'), with: attributes[:last_name]
    end
  end
end
