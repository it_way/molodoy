# frozen_string_literal: true

require 'rails_helper'

describe 'Show main page' do
  before { create :block }

  context 'with close events' do
    let!(:events) do
      Tramway::Event::Event.delete_all
      Tramway::User::User.delete_all
      Tramway::User::User.role.values.reduce({}) do |hash, role|
        create role
        hash.merge! role => {
          open: create("event_created_by_#{role}", reach: :open,
                                                   begin_date: DateTime.tomorrow, end_date: DateTime.now + 10.days),
          closed: create("event_created_by_#{role}", reach: :closed,
                                                     begin_date: DateTime.tomorrow, end_date: DateTime.now + 10.days)
        }
      end
    end

    it 'should show close events from every admin and partner user' do
      visit '/'

      events.each do |pair|
        expect(page).to have_content pair[1][:open].title
        expect(page).to have_content pair[1][:open].short_description.split('.').first
      end
    end

    it 'should not show closed events' do
      visit '/'

      events.each do |pair|
        expect(page).not_to have_content pair[1][:closed].title
        expect(page).not_to have_content pair[1][:closed].short_description.split('.').first
      end
    end
  end
end
