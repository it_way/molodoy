# frozen_string_literal: true

require 'rails_helper'

describe 'Search participants' do
  before { create_list :participant, 5, event: create(:event_created_by_partner) }

  it 'should search participant' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    first_name = Tramway::Event::Participant.last.values['Фамилия']
    click_on 'Участники'
    fill_in 'search', with: first_name
    click_on 'Искать'

    expect(page).to have_content first_name
  end
end
