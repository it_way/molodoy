# frozen_string_literal: true

require 'rails_helper'
require 'roo-xls'

describe 'All scopes exporting spec' do
  context 'should export xls' do
    let!(:event) { create :event_created_by_partner }
    before do
      create_list :participant, 5, event: create(:event_created_by_partner, title: 'event')
      Tramway::Event::Participant.state_machines[:participation_state].states.map(&:name).each do |state|
        create_list :participant, 5, event: event, participation_state: state
      end
    end

    it 'certain participants' do
      visit '/admin'
      fill_in 'Email', with: 'partner@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      click_on 'Мероприятия'
      click_on event.title
      click_on 'Открыть'

      Tramway::Event::ParticipantDecorator.collections.each do |state|
        case state
        when :all
          first('button#export-tramway_event_participant.dropdown-toggle').click.click
          click_on 'Все'
        when :requested
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Необработанные'
        when :waiting
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Ожидаем ответа'
        when :prev_approved
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Предварительно подтверждены'
        when :without_answer
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Не ответили'
        when :approved
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Пришли'
        when :rejected
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'Отклонены'
        when :reserved
          first('button#export-tramway_event_participant.dropdown-toggle').click
          click_on 'В резерве'
        end

        sleep 3
        path_download = File.expand_path('tmp/files')
        filename = `ls -t #{path_download} | grep participant`.split("\n").first
        path = path_download + '/' + filename
        xls = Roo::Spreadsheet.open(path).column(3).to_a
        system('rm "' + path + '"')

        participants_name = []
        case state
        when :all
          event.participants.each do |participant|
            participants_name << participant.values['Имя']
          end
        else
          event.participants.each do |participant|
            participants_name << participant.values['Имя'] if participant.participation_state.eql?(state.to_s)
          end
        end

        assert((xls - (participants_name << 'Имя')).empty?)
      end
    end
  end
end
