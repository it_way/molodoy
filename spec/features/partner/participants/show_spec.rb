# frozen_string_literal: true

require 'rails_helper'

describe 'Show participant' do
  before { create :participant, event: create(:event_created_by_partner) }

  it 'should show participant' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = create :participant
    click_on 'Участник'
    title = "#{participant.values['Фамилия']} #{participant.values['Имя']} #{participant.values['Отчество']}"
    click_on title

    expect(page).to have_content title
  end
end
