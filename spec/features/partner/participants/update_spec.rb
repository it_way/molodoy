# frozen_string_literal: true

require 'rails_helper'

describe 'Update participant' do
  let!(:attributes) { attributes_for :participant_admin_attributes }
  before { create :participant, event: create(:event_created_by_partner) }

  it 'should update participant' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    title = "#{participant.values['Фамилия']} #{participant.values['Имя']} #{participant.values['Отчество']}"
    click_on title
    find('.btn.btn-warning', match: :first).click
    fill_in 'record[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'record[Имя]', with: attributes[:'Имя']
    fill_in 'record[Отчество]', with: attributes[:'Отчество']
    fill_in 'record[Дата рождения]', with: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'record[Муниципальное образование]'
    fill_in 'record[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'record[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'record[Email]', with: attributes[:Email]
    fill_in 'record[Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт]',
      with: attributes[:'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт']
    check 'record[Я согласен на обработку моих персональных данных]'

    click_on 'Сохранить', class: 'btn-success'
    participant.reload
    attributes.each do |pair|
      actual = participant.values[pair[0].to_s]
      expecting = pair[1]
      expecting = expecting.strftime('%d.%m.%Y') if expecting.is_a? DateTime
      expect(actual).to eq(expecting), problem_with(attr: pair[0], expecting: expecting, actual: actual)
    end
  end

  it 'should show participant admin page' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on "#{participant.values['Фамилия']} #{participant.values['Имя']} #{participant.values['Отчество']}"
    find('.btn.btn-warning', match: :first).click
    fill_in 'record[Фамилия]', with: attributes[:'Фамилия']
    fill_in 'record[Имя]', with: attributes[:'Имя']
    fill_in 'record[Отчество]', with: attributes[:'Отчество']
    fill_in 'record[Дата рождения]', with: attributes[:'Дата рождения']
    select attributes[:'Муниципальное образование'], from: 'record[Муниципальное образование]'
    fill_in 'record[Место учёбы / работы]', with: attributes[:'Место учёбы / работы']
    fill_in 'record[Номер телефона]', with: attributes[:'Номер телефона']
    fill_in 'record[Email]', with: attributes[:Email]
    fill_in 'record[Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт]',
      with: attributes[:'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт']
    check 'record[Я согласен на обработку моих персональных данных]'

    click_on 'Сохранить', class: 'btn-success'

    participant.reload
    expect(page).to have_content(
      "#{participant.values['Фамилия']} #{participant.values['Имя']} #{participant.values['Отчество']}"
    )
  end
end
