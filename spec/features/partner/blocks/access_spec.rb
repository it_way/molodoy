# frozen_string_literal: true

require 'rails_helper'

describe 'Admin access' do
  it 'should not have access to block CRUD' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    expect(page).not_to have_content 'Блок'
  end
end
