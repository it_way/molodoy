# frozen_string_literal: true

require 'rails_helper'

describe 'Show social_network' do
  before { create :social_network_created_by_partner }

  it 'should show social_network' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    last_social_network = Tramway::Profiles::SocialNetwork.active.last
    click_on_dropdown 'Администрирование'
    click_on 'Социальные сети'
    click_on last_social_network.title

    expect(page).to have_content last_social_network.title
  end
end
