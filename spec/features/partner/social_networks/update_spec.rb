# frozen_string_literal: true

require 'rails_helper'

describe 'Update social_network' do
  let!(:attributes) { attributes_for :social_network_admin_attributes }
  before { create :social_network_created_by_partner }

  it 'should update social_network' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    social_network = Tramway::Profiles::SocialNetwork.active.last
    click_on_dropdown 'Администрирование'
    click_on 'Социальные сети'
    click_on social_network.title
    find('.btn.btn-warning', match: :first).click
    fill_in 'record[title]', with: attributes[:title]
    fill_in 'record[uid]', with: attributes[:uid]
    select attributes[:network_name], from: 'record[network_name]'
    select attributes[:record], from: 'record[record]'

    click_on 'Сохранить', class: 'btn-success'
    social_network.reload
    attributes.keys.each do |attr|
      actual = social_network.send(attr)
      expecting = attributes[attr]
      case actual.class.to_s
      when 'NilClass'
        expect(actual).not_to be_empty, "#{attr} is empty"
      when 'Enumerize::Value'
        expect(actual).not_to be_empty, "#{attr} is empty"
        actual = actual.text
      when 'Tramway::User::User'
        expect(actual).to be_present, "#{attr} is empty"
        actual = "#{actual.class.model_name.human} | #{actual.title}"
      end
      expect(actual).to eq(expecting), problem_with(attr: attr, expecting: expecting, actual: actual)
    end
  end
end
