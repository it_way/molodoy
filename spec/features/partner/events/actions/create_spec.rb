# frozen_string_literal: true

require 'rails_helper'

describe 'Create action' do
  let!(:event) { create :event_created_by_partner }

  it 'creates new action' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    click_on event.title
    click_on 'Добавить действие'

    attributes = attributes_for :action_partner_attributes
    fill_in 'record[title]', with: attributes[:title]
    fill_in 'record[deadline]', with: attributes[:deadline]

    click_on 'Сохранить'

    request_uri = URI.parse(current_url).request_uri
    expect(request_uri).to eq(
      ::Tramway::Admin::Engine.routes.url_helpers.record_path(event.id, model: 'Tramway::Event::Event')
    )

    last_action = ::Tramway::Event::Action.last
    expect(page).to have_content last_action.title
  end

  it 'should allow to create actions only for partner\'s event' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    click_on event.title
    click_on 'Добавить действие'

    admin_event = create :event_created_by_admin

    expect(page).to have_select 'record[event]', with_options: [event.title]
    expect(page).not_to have_select 'record[event]', with_options: [admin_event.title]
  end
end
