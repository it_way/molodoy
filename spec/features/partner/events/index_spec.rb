# frozen_string_literal: true

require 'rails_helper'

describe 'Index events' do
  let!(:events) { create_list :event_created_by_partner, 5 }

  it 'should show index events page' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'

    events.each do |_event|
      last_event = Tramway::Event::Event.last
      expect(page).to have_content last_event.title
      expect(page).to have_content Tramway::User::UserDecorator.decorate(last_event.creator).name
    end
  end
end
