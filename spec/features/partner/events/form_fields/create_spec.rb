# frozen_string_literal: true

require 'rails_helper'

describe 'Create participant_form_field' do
  let!(:event) { create :event_created_by_partner }

  it 'creates new participant_form_field' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    click_on event.title
    click_on 'Добавить поле анкеты'

    attributes = attributes_for :participant_form_field_admin_attributes
    fill_in 'record[title]', with: attributes[:title]
    fill_in 'record[description]', with: attributes[:description]
    fill_in 'record[position]', with: attributes[:position]
    select attributes[:field_type], from: 'record[field_type]'

    click_on 'Сохранить'

    request_uri = URI.parse(current_url).request_uri
    expect(request_uri).to eq(
      ::Tramway::Admin::Engine.routes.url_helpers.record_path(event.id, model: 'Tramway::Event::Event')
    )

    last_participant_form_field = ::Tramway::Event::ParticipantFormField.last
    expect(page).to have_content last_participant_form_field.title
  end

  it 'should allow to create fields only for partner\'s event' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    click_on event.title
    click_on 'Добавить поле анкеты'

    admin_event = create :event_created_by_admin

    expect(page).to have_select 'record[event]', with_options: [event.title]
    expect(page).not_to have_select 'record[event]', with_options: [admin_event.title]
  end
end
