# frozen_string_literal: true

require 'rails_helper'

describe 'Show event' do
  before { create :event_created_by_partner }

  it 'should show event' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    last_event = create :event_created_by_partner
    click_on 'Мероприятия'
    click_on last_event.title

    expect(page).to have_content last_event.title
  end
end
