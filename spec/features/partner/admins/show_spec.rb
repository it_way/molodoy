# frozen_string_literal: true

require 'rails_helper'

describe 'Show partner' do
  let(:last_partner) { Tramway::User::User.partners.last }

  it 'should show partner' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on_dropdown 'Администрирование'
    click_on 'Администраторы'
    click_on last_partner.id

    expect(page).to have_content last_partner.email
    expect(page).to have_content last_partner.phone
  end

  context 'events' do
    it 'should show partner events' do
      visit '/admin'
      fill_in 'Email', with: 'partner@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      event = create :event_created_by_partner
      click_on_dropdown 'Администрирование'
      click_on 'Администраторы'
      click_on last_partner.id

      expect(page).to have_content event.title
    end

    it 'should show partner events' do
      visit '/admin'
      fill_in 'Email', with: 'partner@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      event = create :event_created_by_partner
      click_on_dropdown 'Администрирование'
      click_on 'Администраторы'
      click_on last_partner.id

      expect(page).to have_content event.title
    end
  end
end
