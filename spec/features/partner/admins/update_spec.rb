# frozen_string_literal: true

require 'rails_helper'

describe 'Update partner' do
  let!(:attributes) { attributes_for :partner_admin_attributes }

  it 'should update partner' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    partner = Tramway::User::User.last
    click_on_dropdown 'Администрирование'
    click_on 'Администраторы'
    click_on partner.id
    find('.btn.btn-warning', match: :first).click
    fill_in 'record[email]', with: attributes[:email]
    fill_in 'record[password]', with: attributes[:password]
    fill_in 'record[first_name]', with: attributes[:first_name]
    fill_in 'record[last_name]', with: attributes[:last_name]
    fill_in 'record[phone]', with: attributes[:phone]

    click_on 'Сохранить', class: 'btn-success'
    partner.reload
    attributes.keys.each do |attr|
      next if attr == :password

      actual = partner.send(attr)
      expecting = attributes[attr]
      case actual.class.to_s
      when 'NilClass'
        expect(actual).not_to be_empty, "#{attr} is empty"
      when 'Enumerize::Value'
        expect(actual).not_to be_empty, "#{attr} is empty"
        actual = actual.text
        if attr == :role
          expect(actual).not_to eq(expecting), problem_with(attr: attr, expecting: expecting, actual: actual)
          next
        end
      end
      expect(actual).to eq(expecting), problem_with(attr: attr, expecting: expecting, actual: actual)
    end
  end
end
