# frozen_string_literal: true

require 'rails_helper'

describe 'Edit partner page' do
  it 'should show edit partner page' do
    visit '/admin'
    fill_in 'Email', with: 'partner@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    last_partner = Tramway::User::User.active.last
    click_on_dropdown 'Администрирование'
    click_on 'Администраторы'
    click_on last_partner.id
    find('.btn.btn-warning', match: :first).click

    expect(page).to have_field 'record[email]', with: last_partner.email
    expect(page).to have_field 'record[first_name]', with: last_partner.first_name
    expect(page).to have_field 'record[last_name]', with: last_partner.last_name
    expect(page).not_to have_selector :css, 'input[name="record[role]"]'
  end
end
