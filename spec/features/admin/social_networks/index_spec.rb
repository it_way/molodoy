# frozen_string_literal: true

require 'rails_helper'

describe 'Index social_network' do
  let!(:social_networks) { create_list :social_network, 5 }

  it 'should index social_network' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on_dropdown 'Администрирование'
    click_on 'Социальные сети'

    social_networks.each do |social_network|
      expect(page).to have_content social_network.title
    end
  end
end
