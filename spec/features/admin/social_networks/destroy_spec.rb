# frozen_string_literal: true

require 'rails_helper'

describe 'Destroy social_network' do
  before { create :social_network }

  it 'should destroy social_network ' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    social_network = Tramway::Profiles::SocialNetwork.last
    click_on_dropdown 'Администрирование'
    click_on 'Социальные сети'
    click_on_delete_button social_network

    social_network.reload
    expect(social_network.removed?).to be_truthy
  end
end
