# frozen_string_literal: true

require 'rails_helper'

describe 'Edit participant page' do
  before { create :participant }

  it 'should show edit participant page' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.active.last
    click_on 'Участник'
    click_on "#{participant.values['Фамилия']} #{participant.values['Имя']} #{participant.values['Отчество']}"
    find('.btn.btn-warning', match: :first).click

    expect(page).to have_field 'record[Фамилия]', with: participant.values['Фамилия']
    expect(page).to have_field 'record[Имя]', with: participant.values['Имя']
    expect(page).to have_field 'record[Отчество]', with: participant.values['Отчество']
    expect(page).to have_field 'record[Дата рождения]', with: participant.values['Дата рождения']
    expect(page).to have_field 'record[Место учёбы / работы]', with: participant.values['Место учёбы / работы']
    expect(page).to have_field 'record[Номер телефона]', with: participant.values['Номер телефона']
    expect(page).to have_field 'record[Email]', with: participant.values['Email']
    region_field_name = 'Если вы не из Ульяновской области, укажите, пожалуйста, свой регион и населённый пункт'
    expect(page).to have_field "record[#{region_field_name}]", with: participant.values[region_field_name]
    expect(page).to have_select(
      'record[Муниципальное образование]', selected: participant.values['Муниципальное образование']
    )
  end
end
