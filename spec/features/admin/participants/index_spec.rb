# frozen_string_literal: true

require 'rails_helper'

describe 'Index participant' do
  let!(:participants) { create_list :participant, 5 }

  it 'should index participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Участники'

    participants.each do |participant|
      expect(page).to have_content Tramway::Event::ParticipantDecorator.decorate(participant).name
    end
  end

  context 'Search' do
    it 'should search participants' do
      visit '/admin'
      fill_in 'Email', with: 'admin@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      click_on 'Участники'
      searching_participant = participants.first
      fill_in 'search', with: searching_participant.values['Email']
      click_on 'Искать'

      expect(page).to have_content Tramway::Event::ParticipantDecorator.decorate(searching_participant).name

      participants[1..-1].each do |participant|
        expect(page).not_to have_content Tramway::Event::ParticipantDecorator.decorate(participant).name
      end
    end

    it 'should show full list if search is empty' do
      visit '/admin'
      fill_in 'Email', with: 'admin@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      click_on 'Участники'
      click_on 'Искать'

      participants.each do |participant|
        expect(page).to have_content Tramway::Event::ParticipantDecorator.decorate(participant).name
      end
    end
  end
end
