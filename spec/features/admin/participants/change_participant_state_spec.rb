# frozen_string_literal: true

require 'rails_helper'

describe 'Change participant state' do
  before { create :participant }

  it 'should pre_approve participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Предварительно подтвердить'

    participant.reload

    expect(participant.prev_approved?).to be_truthy
  end

  it 'should wait for decision participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Ожидать ответа'

    participant.reload

    expect(participant.waiting?).to be_truthy
  end

  it 'should reserve participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Отправить в резерв'

    participant.reload

    expect(participant.reserved?).to be_truthy
  end

  it 'should reject participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Отклонить'

    participant.reload

    expect(participant.rejected?).to be_truthy
  end

  it 'should approve participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Прибыл(а)'

    participant.reload

    expect(participant.approved?).to be_truthy
  end

  it 'should not_got_answer participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участник'
    click_on 'Необработанные'
    click_on 'Связаться не удалось'

    participant.reload

    expect(participant.without_answer?).to be_truthy
  end
end
