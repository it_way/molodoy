# frozen_string_literal: true

require 'rails_helper'

describe 'All scopes exporting spec' do
  before do
    Tramway::Event::Participant.state_machines[:participation_state].states.map(&:name).each do |state|
      create :participant, participation_state: state
    end
  end

  it 'should export xls' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Участник'
    first('button#export-tramway_event_participant.dropdown-toggle').click.click
    click_on 'Все'
    sleep 10
    path_download = File.expand_path('tmp/files')
    filename = `ls -t #{path_download} | grep participant`.split("\n").first
    path = path_download + '/' + filename
    system('rm "' + path + '"')
    expect(File.extname(filename) == '.xls')
  end
end
