# frozen_string_literal: true

require 'rails_helper'

describe 'Index events' do
  let!(:events) { create_list :event_created_by_admin, 5 }

  it 'should show index events page' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'

    events.each do |_event|
      last_event = Tramway::Event::Event.last
      expect(page).to have_content last_event.title
      expect(page).to have_content Tramway::User::UserDecorator.decorate(last_event.creator).name
    end
  end

  context 'show event_link' do
    before { create :event }

    it 'should show event_link' do
      visit '/admin'
      fill_in 'Email', with: 'admin@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      last_event = create :event
      click_on 'Мероприятия'
      click_on last_event.title

      expect(page).to have_content ['http://localhost:3000/events/', last_event.id].join
    end
  end

  context 'participants count' do
    let(:event) { create :event }
    before do
      create_list :participant, 5, event_id: event.id, participation_state: :requested
      create_list :participant, 5, event_id: event.id, participation_state: :prev_approved
    end

    it 'should show requested participants count' do
      visit '/admin'
      fill_in 'Email', with: 'admin@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      click_on 'Мероприятия'

      cell = find('table td a', text: event.title).parent_node(level: 2).all('td')[2]
      expect(cell).to have_content '10'
    end

    it 'should show requested participants count without deleted' do
      event.participants.first(3).each(&:remove)
      visit '/admin'
      fill_in 'Email', with: 'admin@email.com'
      fill_in 'Пароль', with: '123456'
      click_on 'Войти', class: 'btn-success'

      click_on 'Мероприятия'

      cell = find('table td a', text: event.title).parent_node(level: 2).all('td')[2]
      expect(cell).to have_content '7'
    end
  end
end
