# frozen_string_literal: true

require 'rails_helper'

describe 'Destroy participant' do
  before { create :participant }

  it 'should destroy participant' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    participant = Tramway::Event::Participant.last
    click_on 'Участники'
    click_on_delete_button participant
    participant.reload

    expect(participant.removed?).to be_truthy
  end
end
