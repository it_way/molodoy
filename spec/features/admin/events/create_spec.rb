# frozen_string_literal: true

require 'rails_helper'

describe 'Create event' do
  let!(:attributes) { attributes_for :event_admin_attributes }

  it 'should create event' do
    count = Tramway::Event::Event.count
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    find('.btn.btn-primary', match: :first).click
    fill_in 'record[title]', with: attributes[:title]
    click_on_date_picker_date 'record[begin_date]', value: attributes[:begin_date]
    click_on_date_picker_date 'record[end_date]', value: attributes[:end_date]
    click_on_date_picker_date 'record[request_collecting_begin_date]', value: attributes[:request_collecting_begin_date]
    click_on_date_picker_date 'record[request_collecting_end_date]', value: attributes[:request_collecting_end_date]
    select attributes[:reach], from: 'record[reach]'

    click_on 'Сохранить', class: 'btn-success'
    expect(Tramway::Event::Event.count).to eq(count + 1)
    event = Tramway::Event::Event.last
    attributes.keys.each do |attr|
      actual = event.send(attr)
      expecting = attributes[attr]
      if attr.in? %i[begin_date end_date request_collecting_end_date request_collecting_begin_date]
        actual = actual.strftime('%d.%m.%Y')
      end
      case actual.class.to_s
      when 'NilClass'
        expect(actual).not_to be_empty, "#{attr} is empty"
      when 'Enumerize::Value'
        expect(actual).not_to be_empty, "#{attr} is empty"
        actual = actual.text
      end
      expect(actual).to eq(expecting), problem_with(attr: attr, expecting: expecting, actual: actual)
    end
  end

  it 'should not create event without begin date' do
    count = Tramway::Event::Event.count
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    find('.btn.btn-primary', match: :first).click
    fill_in 'record[title]', with: attributes[:title]
    click_on_date_picker_date 'record[end_date]', value: attributes[:end_date]
    click_on_date_picker_date 'record[request_collecting_begin_date]', value: attributes[:request_collecting_begin_date]
    click_on_date_picker_date 'record[request_collecting_end_date]', value: attributes[:request_collecting_end_date]
    select attributes[:reach], from: 'record[reach]'

    click_on 'Сохранить', class: 'btn-success'
    expect(Tramway::Event::Event.count).to eq(count)
    expect(page).to have_content 'Дата начала не может быть пустым'
  end

  it 'should not create event without end date' do
    count = Tramway::Event::Event.count
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    click_on 'Мероприятия'
    find('.btn.btn-primary', match: :first).click
    fill_in 'record[title]', with: attributes[:title]
    click_on_date_picker_date 'record[begin_date]', value: attributes[:begin_date]
    click_on_date_picker_date 'record[request_collecting_begin_date]', value: attributes[:request_collecting_begin_date]
    click_on_date_picker_date 'record[request_collecting_end_date]', value: attributes[:request_collecting_end_date]
    select attributes[:reach], from: 'record[reach]'

    click_on 'Сохранить', class: 'btn-success'
    expect(Tramway::Event::Event.count).to eq(count)
    expect(page).to have_content 'Дата конца не может быть пустым'
  end
end
