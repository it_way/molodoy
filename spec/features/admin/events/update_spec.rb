# frozen_string_literal: true

require 'rails_helper'

describe 'Update event' do
  let!(:attributes) { attributes_for :event_admin_attributes }
  before { create :event }

  it 'should update event' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    event = Tramway::Event::Event.last
    click_on 'Мероприятия'
    click_on event.title
    find('.btn.btn-warning', match: :first).click
    fill_in 'record[title]', with: attributes[:title]
    click_on_date_picker_date 'record[begin_date]', value: attributes[:begin_date]
    click_on_date_picker_date 'record[end_date]', value: attributes[:end_date]
    click_on_date_picker_date 'record[request_collecting_begin_date]', value: attributes[:request_collecting_begin_date]
    click_on_date_picker_date 'record[request_collecting_end_date]', value: attributes[:request_collecting_end_date]
    select attributes[:reach], from: 'record[reach]'

    click_on 'Сохранить', class: 'btn-success'
    event.reload
    attributes.keys.each do |attr|
      actual = event.send(attr)
      expecting = attributes[attr]
      if attr.in? %i[begin_date end_date request_collecting_end_date request_collecting_begin_date]
        actual = actual.strftime('%d.%m.%Y')
      end
      case actual.class.to_s
      when 'NilClass'
        expect(actual).not_to be_empty, "#{attr} is empty"
      when 'Enumerize::Value'
        expect(actual).not_to be_empty, "#{attr} is empty"
        actual = actual.text
      end
      expect(actual).to eq(expecting), problem_with(attr: attr, expecting: expecting, actual: actual)
    end
  end
end
