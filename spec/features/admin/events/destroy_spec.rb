# frozen_string_literal: true

require 'rails_helper'

describe 'Destroy event' do
  before { create :event }

  it 'should destroy event' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    event = Tramway::Event::Event.last
    click_on 'Мероприятия'
    click_on_delete_button event
    event.reload

    expect(event.removed?).to be_truthy
  end
end
