# frozen_string_literal: true

require 'rails_helper'

describe 'Show block' do
  before do
    landing_page = create :page
    create :block, page: landing_page
  end

  it 'should show block' do
    visit '/admin'
    fill_in 'Email', with: 'admin@email.com'
    fill_in 'Пароль', with: '123456'
    click_on 'Войти', class: 'btn-success'

    last_page = Tramway::Page::Page.last
    click_on_dropdown 'Администрирование'
    click_on 'Страницы'
    click_on last_page.title
    last_block = Tramway::Landing::Block.last
    click_on last_block.title

    expect(page).to have_content last_block.title
  end
end
