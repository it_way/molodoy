# frozen_string_literal: true

module DatePickerHelper
  def click_on_date_picker_date(input_name, value:)
    find("input[name='#{input_name}']").click
    value_time = value.to_datetime.to_time
    move_to_needed_month value_time
    find('td.day:not(.old):not(.new)', exact_text: value.split('.').first.to_i.to_s).click
  end

  private

  def move_to_needed_month(value_time)
    difference = TimeDifference.between(value_time, Time.now).in_months
    difference = DateTime.now.day < 15 ? difference.round.floor : difference.floor
    if value_time > Time.now
      difference.times { find('th.next').click }
    elsif value_time < Time.now
      difference.times { find('th.prev').click }
    end
  end
end
