# frozen_string_literal: true

require_relative 'capybara_helpers/node'

module CapybaraHelpers
  include Node
end
