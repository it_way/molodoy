# frozen_string_literal: true

require_relative 'node/element'

module CapybaraHelpers::Node
  include Element
end
